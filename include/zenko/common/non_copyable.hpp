#pragma once

namespace zk
{
class NonCopyable
{
  public:
    virtual ~NonCopyable() = default;
    NonCopyable(const NonCopyable&) = delete;
    NonCopyable& operator=(const NonCopyable&) = delete;

  protected:
    explicit NonCopyable() = default;
};
}  // namespace zk
