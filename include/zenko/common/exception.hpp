#pragma once

#include <stdexcept>
#include <string>

namespace zk
{
class Exception final : public std::exception
{
  public:
    explicit Exception(const char* message) : mMessage(message), mCause(std::current_exception()) {}
    explicit Exception(const std::string& message) : mMessage(message), mCause(std::current_exception()) {}

    virtual ~Exception() = default;

    inline virtual const char* what() const noexcept { return mMessage.c_str(); };
    inline const std::exception_ptr& cause() const noexcept { return mCause; };

  private:
    const std::string mMessage;
    const std::exception_ptr mCause;
};
}  // namespace zk
