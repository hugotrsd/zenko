#pragma once

#include <zenko/common/non_copyable.hpp>

#include <memory>
#include <string>

namespace zk
{
class Exception;
class Logger : private NonCopyable
{
  public:
    enum Level {
        TRACE    = 0,
        DEBUG    = 1,
        INFO     = 2,
        WARNING  = 3,
        ERROR    = 4,
        CRITICAL = 5,
        OFF      = 6
    };

  public:
    virtual ~Logger();

    static std::unique_ptr<Logger> create(const std::string& name);

    virtual Level level() const noexcept             = 0;
    virtual const std::string& name() const noexcept = 0;

    virtual void setLevel(Level level) = 0;

    virtual void log(Level level, const std::string& message) const noexcept = 0;
    virtual void log(Level level, const Exception& ex) const noexcept        = 0;

  protected:
    explicit Logger();
};
}  // namespace zk
