#pragma once

#include <zenko/common/non_copyable.hpp>

namespace zk
{
class Application;
class Surface : private NonCopyable
{
  public:
    virtual ~Surface();

  protected:
    explicit Surface(const Application& app);

  protected:
    const Application& mApp;
};
}  // namespace zk
