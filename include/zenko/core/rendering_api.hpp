#pragma once

#include <zenko/common/non_copyable.hpp>

namespace zk
{
class Application;
class Renderer;
class RenderingAPI : private NonCopyable
{
  public:
    enum Impl {
        NONE = 0,
        VULKAN,
    };

  public:
    virtual ~RenderingAPI();

    virtual constexpr Impl impl() const noexcept = 0;

  protected:
    explicit RenderingAPI(const Application& app);

  protected:
    const Application& mApp;
};
}  // namespace zk
