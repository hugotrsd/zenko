#pragma once

#include <zenko/common/non_copyable.hpp>

namespace zk
{
class RendererCTX;
class Surface;
class Renderable : private NonCopyable
{
  public:
    virtual ~Renderable() = default;

    virtual const RendererCTX& rendererCTX() const noexcept = 0;
    virtual const Surface& surface() const noexcept = 0;

  protected:
    explicit Renderable() = default;
};
}  // namespace zk
