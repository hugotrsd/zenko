#pragma once

#include <zenko/common/non_copyable.hpp>
#include <zenko/core/renderable.hpp>

#include <memory>
#include <string>

namespace zk
{
class Application;
class Surface;
class Window : public Renderable
{
  public:
    struct Properties
    {
        std::string title = "Zenko";
        int width         = 1280;
        int height        = 720;
        bool fullscreen   = false;
        bool maximized    = false;
        bool borderless   = false;
    };

  public:
    virtual ~Window();

    static std::unique_ptr<Window> create(const Application& app, const Properties& props);

  protected:
    explicit Window(const Application& app);

  protected:
    const Application& mApp;
};
}  // namespace zk
