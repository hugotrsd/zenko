#pragma once

#include <zenko/common/non_copyable.hpp>

namespace zk
{
class Application;
class RendererCTX : private NonCopyable
{
  public:
    virtual ~RendererCTX();

  protected:
    explicit RendererCTX(const Application& app);

  protected:
    const Application& mApp;
};
}  // namespace zk
