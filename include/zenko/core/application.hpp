#pragma once

#include <zenko/common/non_copyable.hpp>

#include <memory>
#include <mutex>

namespace zk
{
class Logger;
class RenderingAPI;
class Application : private NonCopyable
{
  private:  // At the top to make sure it's created first
    static std::mutex sMutex;  // Prevent creation of concurrent instances
    std::scoped_lock<std::mutex> mLock;

  public:
    enum Impl {
        NONE = 0,
        SDL2,
    };

  public:
    virtual ~Application();

    static std::unique_ptr<Application> create(int argc, const char* argv[]);

    const Logger& logger() const noexcept;

    virtual constexpr Impl impl() const noexcept              = 0;
    virtual const RenderingAPI& renderingAPI() const noexcept = 0;

  protected:
    explicit Application();

  private:
    std::unique_ptr<Logger> mLogger;
};
}  // namespace zk
