#pragma once

#include <zenko/common/non_copyable.hpp>

#include <memory>

namespace zk
{
class Application;
class Renderable;
class Renderer : private NonCopyable
{
  public:
    virtual ~Renderer();

    virtual void attach(const Renderable& renderable) = 0;
    virtual void detach()                             = 0;

  protected:
    explicit Renderer(const Application& app);

  protected:
    const Application& mApp;
};
}  // namespace zk
