#include "backend/vulkan/vulkan.hpp"
#include "main.hpp"

#include <zenko/common/exception.hpp>
#include <zenko/core/application.hpp>
#include <zenko/core/logger.hpp>

#include <fmt/core.h>

#include <algorithm>
#include <cstring>

namespace zk
{
std::vector<const char*> checkExtensionsSupport(const std::vector<const char*>& required)
{
    auto supported = vk::enumerateInstanceExtensionProperties();

    std::vector<const char*> unsupported;
    for (auto& extension : required) {
        auto it = std::find_if(
            supported.begin(),
            supported.end(),
            [&](const vk::ExtensionProperties& a) { return std::strcmp(a.extensionName, extension) == 0; });

        if (it == supported.end()) unsupported.push_back(extension);
    }

    return unsupported;
}

void Vulkan::initExtensions(const std::vector<const char*>& requiredExtensions)
{
    ZK_ASSERT(mEnabledExtensions.empty());

    std::vector<const char*> required(requiredExtensions);
    if (mDebug) {
        mApp.logger().log(Logger::Level::DEBUG, "requesting vulkan debug extensions");
        if (std::find_if(
                required.begin(),
                required.end(),
                [&](const char* lay) { return std::strcmp(VK_EXT_DEBUG_UTILS_EXTENSION_NAME, lay) == 0; })
            == required.end()) {
                required.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
            }
    }

    auto unsupported = checkExtensionsSupport(required);
    if (!unsupported.empty())
        throw Exception("some vulkan extensions are not supported");

    mEnabledExtensions = required;
}
}  // namespace zk
