#include "backend/vulkan/vulkan_device.hpp"
#include "backend/vulkan/surface_impl.hpp"
#include "backend/vulkan/vulkan.hpp"
#include "main.hpp"

#include <zenko/common/exception.hpp>
#include <zenko/core/application.hpp>
#include <zenko/core/logger.hpp>

#include <fmt/core.h>
#include <fmt/ranges.h>

#include <cstdint>
#include <map>
#include <set>

namespace zk
{
// Returns true if the physical device supports all extensions
bool checkDeviceExtensionsSupport(const vk::PhysicalDevice& device, const std::vector<const char*>& extensions)
{
    ZK_ASSERT(device);
    std::vector<vk::ExtensionProperties> supported = device.enumerateDeviceExtensionProperties();

    std::vector<const char*> unsupported;
    for (auto extension : extensions) {
        auto it = std::find_if(
            supported.begin(),
            supported.end(),
            [&](const vk::ExtensionProperties& ext) { return std::strcmp(ext.extensionName, extension) == 0; });

        if (it == supported.end()) unsupported.push_back(extension);
    }

    return unsupported.empty();
}

VulkanDevice::QueuesDetails getPhysicalDeviceQueuesDetails(vk::PhysicalDevice device, vk::SurfaceKHR surface)
{
    ZK_ASSERT(device);
    ZK_ASSERT(surface);

    auto properties = device.getQueueFamilyProperties();
    ZK_ASSERT(!properties.empty());

    VulkanDevice::QueuesDetails tr = {};
    for (size_t i = 0; i < properties.size(); ++i) {
        if (properties[i].queueFlags & vk::QueueFlagBits::eGraphics)
            tr.graphicsFamilyIndex = i;

        if (properties[i].queueFlags & vk::QueueFlagBits::eCompute)
            tr.computeFamilyIndex = i;

        if (device.getSurfaceSupportKHR(i, surface) == VK_TRUE)
            tr.presentFamilyIndex = i;

        if (properties[i].queueFlags & vk::QueueFlagBits::eTransfer)
            tr.transferFamilyIndex = i;
    }

    // Specification says if transfer is not set, set it to graphics
    if (!tr.transferFamilyIndex.has_value() && tr.graphicsFamilyIndex.has_value()) {
        tr.transferFamilyIndex = tr.graphicsFamilyIndex;
    }

    return tr;
}

// /!\ This requires the swapchain extension to be supported
VulkanDevice::SwapchainDetails getPhysicalDeviceSwapchainDetails(vk::PhysicalDevice device, vk::SurfaceKHR surface)
{
    ZK_ASSERT(device);
    ZK_ASSERT(surface);

    return {
        device.getSurfaceCapabilitiesKHR(surface),
        device.getSurfaceFormatsKHR(surface),
        device.getSurfacePresentModesKHR(surface),
    };
}

vk::PhysicalDevice selectPhysicalDevice(
    const std::vector<vk::PhysicalDevice>& devices,
    const vk::Instance& instance,
    const vk::SurfaceKHR& surface,
    const std::vector<const char*>& requiredExtensions)
{
    // Gives a score to each physical device, and returns the
    // best one or VK_NULL_HANDLE if no device is suitable.

    ZK_ASSERT(instance);
    ZK_ASSERT(surface);

    std::multimap<unsigned long long, vk::PhysicalDevice> sortedDevices;

    // TODO: device selection can be improved or user enforced
    for (const auto& device : devices) {
        vk::PhysicalDeviceProperties dp = device.getProperties();
        vk::PhysicalDeviceFeatures df   = device.getFeatures();

        // Mandatory criterias here
        if (!checkDeviceExtensionsSupport(device, requiredExtensions)) continue;
        if (!getPhysicalDeviceQueuesDetails(device, surface).isSufficient()) continue;
        if (!getPhysicalDeviceSwapchainDetails(device, surface).isSufficient()) continue;
        if (!df.geometryShader) continue;

        // Compute score and add
        unsigned long long score = 0;
        score += dp.limits.maxImageDimension2D;
        if (dp.deviceType == vk::PhysicalDeviceType::eDiscreteGpu) score += 1000;
        sortedDevices.insert({score, device});
    }

    auto it = sortedDevices.rbegin();  // Valid if !sortedDevices.empty(), checked below
    return !sortedDevices.empty() && it->first > 0 ? it->second : nullptr;
}

VulkanDevice::VulkanDevice(const Application& app, const Vulkan& vulkan, const SurfaceImplVulkan& surface)
    : mApp(app), mVulkan(vulkan), mSurface(surface)
{
    initExtensions();

    mApp.logger().log(
        Logger::Level::DEBUG,
        fmt::format(
            "{} vulkan device extension(s) required: {}",
            mExtensions.size(),
            fmt::join(mExtensions, ", ")));

    // Select a physical device
    auto physicalDevices = mVulkan.handle().enumeratePhysicalDevices();
    if (physicalDevices.empty())
        throw Exception("failed to find a physical device with vulkan support");

    mPhysicalDevice = selectPhysicalDevice(physicalDevices, mVulkan.handle(), mSurface.handle(), mExtensions);
    if (!mPhysicalDevice)
        throw Exception("failed to find a suitable vulkan physical device");

    // From here, we have a suitable device
    ZK_ASSERT(mPhysicalDevice);

    mApp.logger().log(
        Logger::Level::INFO,
        fmt::format(
            "using graphics processing unit {}",
            mPhysicalDevice.getProperties().deviceName));

    mQueuesDetails = std::make_unique<VulkanDevice::QueuesDetails>(getPhysicalDeviceQueuesDetails(mPhysicalDevice, mSurface.handle()));
    ZK_ASSERT(mQueuesDetails->isSufficient());

    mSwapchainDetails = std::make_unique<VulkanDevice::SwapchainDetails>(getPhysicalDeviceSwapchainDetails(mPhysicalDevice, mSurface.handle()));
    ZK_ASSERT(mSwapchainDetails->isSufficient());

    mApp.logger().log(
        Logger::Level::DEBUG,
        fmt::format(
            "device queues indices: graphics({}), transfer({}), presentation({}), compute({})",
            mQueuesDetails->graphicsFamilyIndex.value(),
            mQueuesDetails->transferFamilyIndex.value(),
            mQueuesDetails->presentFamilyIndex.value(),
            mQueuesDetails->computeFamilyIndex.value()));

    // Create queues -> group families using a set to avoid
    // creating multiple queue objects for the same queue
    std::vector<vk::DeviceQueueCreateInfo> queuesInfo;
    std::set<uint32_t> uniqueQueueFamilies = {
        mQueuesDetails->graphicsFamilyIndex.value(),
        mQueuesDetails->presentFamilyIndex.value(),
        mQueuesDetails->computeFamilyIndex.value(),
        mQueuesDetails->transferFamilyIndex.value(),
    };
    float queuesPriority = 1.0f;
    for (auto queueIndex : uniqueQueueFamilies) {
        queuesInfo.push_back({
            {},
            queueIndex,
            1,
            &queuesPriority,
        });
    }

    vk::DeviceCreateInfo deviceInfo = {
        {},
        static_cast<std::uint32_t>(queuesInfo.size()),
        queuesInfo.data(),

        // Device layers and instance layers are combined in
        // recent implementations, so these settings are legacy
        static_cast<std::uint32_t>(vulkan.enabledLayers().size()),
        vulkan.enabledLayers().data(),

        static_cast<std::uint32_t>(mExtensions.size()),
        mExtensions.data(),
        nullptr,
    };

    mDevice                       = mPhysicalDevice.createDevice(deviceInfo);
    mQueuesDetails->graphicsQueue = mDevice.getQueue(mQueuesDetails->graphicsFamilyIndex.value(), 0);
    mQueuesDetails->presentQueue  = mDevice.getQueue(mQueuesDetails->presentFamilyIndex.value(), 0);
    mQueuesDetails->computeQueue  = mDevice.getQueue(mQueuesDetails->computeFamilyIndex.value(), 0);
    mQueuesDetails->transferQueue = mDevice.getQueue(mQueuesDetails->transferFamilyIndex.value(), 0);
}

VulkanDevice::~VulkanDevice()
{
    mDevice.destroy();
}

const vk::Device& VulkanDevice::handle() const noexcept
{
    ZK_ASSERT(mDevice);
    return mDevice;
}

const VulkanDevice::QueuesDetails& VulkanDevice::queuesDetails() const noexcept
{
    ZK_ASSERT(mQueuesDetails != nullptr);
    return *mQueuesDetails;
}

const VulkanDevice::SwapchainDetails& VulkanDevice::swapchainDetails() const noexcept
{
    ZK_ASSERT(mSwapchainDetails != nullptr);
    return *mSwapchainDetails;
}

void VulkanDevice::initExtensions()
{
    ZK_ASSERT(mExtensions.empty());

    mExtensions.push_back(VK_KHR_SWAPCHAIN_EXTENSION_NAME);
}
}  // namespace zk
