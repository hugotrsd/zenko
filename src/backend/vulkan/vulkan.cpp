#include "backend/vulkan/vulkan.hpp"
#include "main.hpp"

#include <zenko/core/application.hpp>
#include <zenko/core/logger.hpp>

#include <fmt/core.h>
#include <fmt/ranges.h>

#include <cstdint>

namespace zk
{
Vulkan::Vulkan(const Application& app, const std::vector<const char*>& extensions)
    : mApp(app)
{
#ifdef ZK_DEBUG
    mDebug = true;
#endif

    initLayers();

    mApp.logger().log(Logger::Level::DEBUG, fmt::format("{} vulkan layer(s) required: {}", mEnabledLayers.size(), fmt::join(mEnabledLayers, ", ")));

    initExtensions(extensions);

    mApp.logger().log(Logger::Level::DEBUG, fmt::format("{} vulkan instance extension(s) required: {}", mEnabledExtensions.size(), fmt::join(mEnabledExtensions, ", ")));

    vk::ApplicationInfo appInfo = {
        ZK_NAME,
        VK_MAKE_VERSION(ZK_VERSION_MAJOR, ZK_VERSION_MINOR, ZK_VERSION_PATCH),
        ZK_NAME,
        VK_MAKE_VERSION(ZK_VERSION_MAJOR, ZK_VERSION_MINOR, ZK_VERSION_PATCH),
        VK_API_VERSION_1_3,
    };

    vk::InstanceCreateInfo instanceInfo = {
        {},
        &appInfo,
        static_cast<std::uint32_t>(mEnabledLayers.size()),
        mEnabledLayers.empty() ? nullptr : mEnabledLayers.data(),
        static_cast<std::uint32_t>(mEnabledExtensions.size()),
        mEnabledExtensions.empty() ? nullptr : mEnabledExtensions.data(),
    };

    // Instance creation time debug messenger
    auto instanceDM = makeDebugMessenger(this);
    if (mDebug) instanceInfo.setPNext((void*)&instanceDM);

    mHandle = vk::createInstance(instanceInfo);

    initDebugMessengers();
}

Vulkan::~Vulkan()
{
    destroyDebugMessengers();

    mHandle.destroy();
}

const vk::Instance& Vulkan::handle() const noexcept
{
    ZK_ASSERT(mHandle);
    return mHandle;
}

const std::vector<const char*>& Vulkan::enabledLayers() const noexcept
{
    ZK_ASSERT(!mEnabledLayers.empty());
    return mEnabledLayers;
}

const std::vector<const char*>& Vulkan::enabledExtensions() const noexcept
{
    ZK_ASSERT(!mEnabledExtensions.empty());
    return mEnabledExtensions;
}
}  // namespace zk
