#pragma once

#include <zenko/common/non_copyable.hpp>

#include <vulkan/vulkan.hpp>

#include <optional>
#include <vector>

namespace zk
{
class Application;
class Vulkan;
class SurfaceImplVulkan;
class VulkanDevice final : private NonCopyable
{
  public:
    struct QueuesDetails;
    struct SwapchainDetails;

  public:
    VulkanDevice(const Application& app, const Vulkan& vulkan, const SurfaceImplVulkan& surface);
    ~VulkanDevice();

    inline const std::vector<const char*>& extensions() const noexcept;
    const vk::Device& handle() const noexcept;
    const QueuesDetails& queuesDetails() const noexcept;
    const SwapchainDetails& swapchainDetails() const noexcept;

  private:
    void initExtensions();

  private:
    const Application& mApp;
    const Vulkan& mVulkan;
    const SurfaceImplVulkan& mSurface;

    vk::Device mDevice;
    vk::PhysicalDevice mPhysicalDevice;

    std::vector<const char*> mExtensions;

    std::unique_ptr<QueuesDetails> mQueuesDetails;
    std::unique_ptr<SwapchainDetails> mSwapchainDetails;
};

const std::vector<const char*>& VulkanDevice::extensions() const noexcept { return mExtensions; }

struct VulkanDevice::QueuesDetails
{
    std::optional<uint32_t> graphicsFamilyIndex;
    vk::Queue graphicsQueue;

    std::optional<uint32_t> presentFamilyIndex;
    vk::Queue presentQueue;

    std::optional<uint32_t> computeFamilyIndex;
    vk::Queue computeQueue;

    // This queue is optional, see spec for details
    // If not set, should take value of graphics or compute family
    std::optional<uint32_t> transferFamilyIndex;
    vk::Queue transferQueue;

    inline bool isSufficient() const noexcept
    {
        return graphicsFamilyIndex.has_value()
               && presentFamilyIndex.has_value()
               && computeFamilyIndex.has_value();
    }
};

struct VulkanDevice::SwapchainDetails
{
    vk::SurfaceCapabilitiesKHR surfaceCapabilities;
    std::vector<vk::SurfaceFormatKHR> surfaceFormats;
    std::vector<vk::PresentModeKHR> presentModes;

    inline bool isSufficient() const noexcept
    {
        return !surfaceFormats.empty()
               && !presentModes.empty();
    }
};
}  // namespace zk
