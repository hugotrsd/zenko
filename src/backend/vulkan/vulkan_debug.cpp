#include "backend/vulkan/vulkan.hpp"
#include "main.hpp"

#include <zenko/common/exception.hpp>
#include <zenko/core/application.hpp>
#include <zenko/core/logger.hpp>

#include <fmt/core.h>

#include <vector>

namespace zk
{
void Vulkan::initDebugMessengers()
{
    ZK_ASSERT(mHandle);
    ZK_ASSERT(mDebugMessengers.empty());

    if (!mDebug) return;

    VkDebugUtilsMessengerCreateInfoEXT createInfo = makeDebugMessenger(this);

    // Load function ar runtime
    auto cdum = reinterpret_cast<PFN_vkCreateDebugUtilsMessengerEXT>(
        mHandle.getProcAddr("vkCreateDebugUtilsMessengerEXT"));

    VkDebugUtilsMessengerEXT dmesgr;
    VkResult res;
    res = cdum(mHandle, &createInfo, nullptr, &dmesgr);

    if (res != VK_SUCCESS) {
        throw Exception("failed to create vulkan debug messenger");
    }

    mDebugMessengers.push_back(dmesgr);
}

void Vulkan::destroyDebugMessengers() noexcept
{
    if (!mDebugMessengers.empty()) {
        // Force load function
        auto ddum = reinterpret_cast<PFN_vkDestroyDebugUtilsMessengerEXT>(
            mHandle.getProcAddr("vkDestroyDebugUtilsMessengerEXT"));

        for (auto& dmesgr : mDebugMessengers)
            ddum(mHandle, dmesgr, nullptr);

        mDebugMessengers.clear();
    }
}

vk::DebugUtilsMessengerCreateInfoEXT Vulkan::makeDebugMessenger(void* pUserData) const noexcept
{
    return vk::DebugUtilsMessengerCreateInfoEXT(
        {},
        vk::DebugUtilsMessageSeverityFlagBitsEXT::eVerbose
            | vk::DebugUtilsMessageSeverityFlagBitsEXT::eInfo
            | vk::DebugUtilsMessageSeverityFlagBitsEXT::eWarning
            | vk::DebugUtilsMessageSeverityFlagBitsEXT::eError,
        vk::DebugUtilsMessageTypeFlagBitsEXT::eGeneral
            | vk::DebugUtilsMessageTypeFlagBitsEXT::eValidation
            | vk::DebugUtilsMessageTypeFlagBitsEXT::ePerformance,
        Vulkan::debugMessengersCallback,
        pUserData);
}

VKAPI_ATTR VkBool32 VKAPI_CALL Vulkan::debugMessengersCallback(
    VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
    VkDebugUtilsMessageTypeFlagsEXT messageType,
    const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
    void* pUserData)
{
    (void)messageType;

    Logger::Level level = Logger::Level::OFF;
    switch (messageSeverity) {
        //case VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT: level = Logger::Level::DEBUG; break;
        case VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT: level = Logger::Level::INFO; break;
        case VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT: level = Logger::Level::WARNING; break;
        case VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT: level = Logger::Level::ERROR; break;
        default: break;
    }

    if (level != Logger::Level::OFF) {
        (static_cast<Vulkan*>(pUserData))->mApp.logger().log(level, fmt::format("[Vulkan] {}", pCallbackData->pMessage));
    }

    return VK_FALSE;
}
}  // namespace zk
