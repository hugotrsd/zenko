#include "backend/vulkan/vulkan_swapchain.hpp"
#include "backend/vulkan/renderer_ctx_impl.hpp"
#include "backend/vulkan/surface_impl.hpp"
#include "backend/vulkan/vulkan_device.hpp"
#include "main.hpp"

#include <zenko/common/exception.hpp>
#include <zenko/core/application.hpp>
#include <zenko/core/logger.hpp>

#include <fmt/core.h>

#include <algorithm>
#include <cstdint>

namespace zk
{
VulkanSwapchain::VulkanSwapchain(
    const Application& app,
    const RendererCTXImplVulkan& rendererCTX,
    const SurfaceImplVulkan& surface,
    const vk::Extent2D& extent2D)
    : mApp(app),
      mSurface(surface),
      mRendererCTX(rendererCTX)
{
    mApp.logger().log(Logger::Level::TRACE, "creating vulkan swapchain");

    // TODO: options
    VulkanDevice::QueuesDetails qd = mRendererCTX.device().queuesDetails();
    ZK_ASSERT(qd.isSufficient());

    VulkanDevice::SwapchainDetails sd = mRendererCTX.device().swapchainDetails();
    ZK_ASSERT(sd.isSufficient());

    vk::SurfaceFormatKHR surfaceFormat = selectSurfaceFormat(sd.surfaceFormats);
    vk::PresentModeKHR presentMode     = selectPresentMode(sd.presentModes, true);
    vk::Extent2D extent                = selectExtent(sd.surfaceCapabilities, extent2D);

    mApp.logger().log(Logger::Level::TRACE, fmt::format("vulkan swapchain extent is ({}, {})", extent.width, extent.height));

    // Ideally we want at least 3 for triple-buffering
    std::uint32_t imgCount = std::max(sd.surfaceCapabilities.minImageCount, 3u);
    if (sd.surfaceCapabilities.maxImageCount != 0) {
        // Hardware has an upper limit, so make sure we don't overshoot
        imgCount = std::clamp(
            imgCount,
            sd.surfaceCapabilities.minImageCount,
            sd.surfaceCapabilities.maxImageCount);
    }

    mApp.logger().log(Logger::Level::TRACE, fmt::format("requesting at least {} images for vulkan swapchain", imgCount));

    std::uint32_t queueFamilyIndices[] = {
        qd.graphicsFamilyIndex.value(),
        qd.presentFamilyIndex.value(),
    };

    vk::SharingMode sharingMode = vk::SharingMode::eConcurrent;
    if (qd.graphicsFamilyIndex.value() == qd.presentFamilyIndex.value()) {
        // Both queues are the same, we can use exclusive mode
        sharingMode = vk::SharingMode::eExclusive;
    }

    vk::SwapchainCreateInfoKHR swapchainInfo = {
        {},
        mSurface.handle(),
        imgCount,
        surfaceFormat.format,
        surfaceFormat.colorSpace,
        extent,
        1,
        vk::ImageUsageFlagBits::eColorAttachment,
        sharingMode,
        sizeof(queueFamilyIndices) / sizeof(std::uint32_t),
        queueFamilyIndices,
        sd.surfaceCapabilities.currentTransform,
        vk::CompositeAlphaFlagBitsKHR::eOpaque,
        presentMode,
        VK_TRUE,
        nullptr,
    };

    mHandle = mRendererCTX.device().handle().createSwapchainKHR(swapchainInfo);

    ZK_ASSERT(mHandle);

    mFormat = surfaceFormat.format;
    mExtent = extent;
    mImages = mRendererCTX.device().handle().getSwapchainImagesKHR(mHandle);

    mApp.logger().log(Logger::Level::TRACE, fmt::format("created vulkan swapchain with {} images", mImages.size()));

    mImageViews.reserve(mImages.size());
    for (const auto& i : mImages) {
        vk::ImageViewCreateInfo viewsInfo = {
            {},
            i,
            vk::ImageViewType::e2D,
            surfaceFormat.format,
            vk::ComponentMapping(
                vk::ComponentSwizzle::eIdentity,
                vk::ComponentSwizzle::eIdentity,
                vk::ComponentSwizzle::eIdentity,
                vk::ComponentSwizzle::eIdentity),
            vk::ImageSubresourceRange(
                vk::ImageAspectFlagBits::eColor,
                0,
                1,
                0,
                1),
        };
        mImageViews.push_back(mRendererCTX.device().handle().createImageView(viewsInfo));
    }
}

VulkanSwapchain::~VulkanSwapchain()
{
    mApp.logger().log(Logger::Level::TRACE, "destroying vulkan swapchain");

    for (const auto& i : mImageViews) {
        mRendererCTX.device().handle().destroyImageView(i);
    }

    mRendererCTX.device().handle().destroySwapchainKHR(mHandle);
}

vk::SurfaceFormatKHR VulkanSwapchain::selectSurfaceFormat(const std::vector<vk::SurfaceFormatKHR>& formats) const noexcept
{
    for (const auto& f : formats) {
        // If available, we want non-linear SRGB
        if (f.format != vk::Format::eB8G8R8A8Srgb) continue;  // VK_FORMAT_B8G8R8A8_SRGB
        if (f.colorSpace != vk::ColorSpaceKHR::eSrgbNonlinear) continue;  // VK_COLOR_SPACE_SRGB_NONLINEAR_KHR
        return f;
    }

    // Specification says > 1, so return the first one
    ZK_ASSERT(!formats.empty());
    return formats.at(0);
}

vk::PresentModeKHR VulkanSwapchain::selectPresentMode(const std::vector<vk::PresentModeKHR>& modes, bool vSync) const noexcept
{
    // For vSync, we prefer VK_PRESENT_MODE_MAILBOX_KHR because it allows for triple buffering
    vk::PresentModeKHR preferedVSync    = vk::PresentModeKHR::eMailbox;
    vk::PresentModeKHR preferedNonVSync = vk::PresentModeKHR::eImmediate;
    for (const auto& m : modes) {
        if ((vSync && m == preferedVSync) || (!vSync && m == preferedNonVSync))
            return m;
    }

    // Only VK_PRESENT_MODE_FIFO_KHR is guaranteed to be available
    return vk::PresentModeKHR::eFifo;
}

vk::Extent2D VulkanSwapchain::selectExtent(const vk::SurfaceCapabilitiesKHR& capabilities, const vk::Extent2D& extent2D) const noexcept
{
    if (capabilities.currentExtent == vk::Extent2D(0xFFFFFFFF, 0xFFFFFFFF)) {
        // Ensure that the desired extent is actually supported by the hardware
        vk::Extent2D tr = extent2D;
        tr.width        = std::clamp(extent2D.width, capabilities.minImageExtent.width, capabilities.maxImageExtent.width);
        tr.height       = std::clamp(extent2D.height, capabilities.minImageExtent.height, capabilities.maxImageExtent.height);
        return tr;
    } else {
        // Adjust swapchain to already defined surface extent
        return capabilities.currentExtent;
    }
}
}  // namespace zk
