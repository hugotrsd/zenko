#include "backend/vulkan/rendering_api_impl.hpp"
#include "backend/vulkan/vulkan.hpp"
#include "main.hpp"

#include <zenko/core/application.hpp>
#include <zenko/core/logger.hpp>

namespace zk
{
RenderingAPIImplVulkan::RenderingAPIImplVulkan(const Application& app, const std::vector<const char*>& extensions)
    : RenderingAPI(app)
{
    mApp.logger().log(Logger::Level::TRACE, "creating vulkan renderer API");

    mVulkan = std::make_unique<Vulkan>(app, extensions);
}

RenderingAPIImplVulkan::~RenderingAPIImplVulkan()
{
    mApp.logger().log(Logger::Level::TRACE, "destroying vulkan renderer API");
}

const Vulkan& RenderingAPIImplVulkan::vulkan() const noexcept
{
    ZK_ASSERT(mVulkan != nullptr);
    return *mVulkan;
}

}  // namespace zk
