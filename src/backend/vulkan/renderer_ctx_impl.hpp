#pragma once

#include <zenko/core/renderer_ctx.hpp>

#include <vulkan/vulkan.hpp>

#include <memory>

namespace zk
{
class Vulkan;
class VulkanDevice;
class VulkanSwapchain;
class SurfaceImplVulkan;
class RendererCTXImplVulkan final : public RendererCTX
{
  public:
    // Will take ownership of the surface
    explicit RendererCTXImplVulkan(const Application& app, const SurfaceImplVulkan& surface, const vk::Extent2D& extent2D);
    virtual ~RendererCTXImplVulkan();

    const VulkanDevice& device() const noexcept;
    const VulkanSwapchain& swapchain() const noexcept;

  private:
    const Vulkan& mVulkan;
    const SurfaceImplVulkan& mSurface;

    std::unique_ptr<VulkanDevice> mDevice;
    std::unique_ptr<VulkanSwapchain> mSwapchain;
};
}  // namespace zk
