#pragma once

#include <zenko/core/rendering_api.hpp>

#include <memory>
#include <vector>

namespace zk
{
class Vulkan;
class RenderingAPIImplVulkan final : public RenderingAPI
{
  public:
    explicit RenderingAPIImplVulkan(const Application& app, const std::vector<const char*>& extensions);
    virtual ~RenderingAPIImplVulkan();

    const Vulkan& vulkan() const noexcept;

    inline virtual constexpr RenderingAPI::Impl impl() const noexcept;

  private:
    std::unique_ptr<Vulkan> mVulkan;
};
RenderingAPI::Impl constexpr RenderingAPIImplVulkan::impl() const noexcept { return RenderingAPI::Impl::VULKAN; }
}  // namespace zk
