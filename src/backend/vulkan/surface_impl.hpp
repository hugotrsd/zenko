#pragma once

#include <zenko/core/surface.hpp>

#include <vulkan/vulkan.hpp>

namespace zk
{
class Vulkan;

// Takes ownership of a vk::SurfaceKHR, since it's created by and tied to the window
// Destruction of vk::SurfaceKHR is not handled by the window
class SurfaceImplVulkan : public Surface
{
  public:
    explicit SurfaceImplVulkan(const Application& app, const Vulkan& vulkan, const vk::SurfaceKHR& surface);
    virtual ~SurfaceImplVulkan();

    const vk::SurfaceKHR& handle() const noexcept;

  private:
    const Vulkan& mVulkan;
    vk::SurfaceKHR mVkSurface;
};
}  // namespace zk
