#include "backend/vulkan/vulkan.hpp"
#include "main.hpp"

#include <zenko/common/exception.hpp>
#include <zenko/core/application.hpp>
#include <zenko/core/logger.hpp>

#include <algorithm>
#include <cstring>

namespace zk
{
// Check if every required layer is supported
std::vector<const char*> checkLayersSupport(const std::vector<const char*>& required)
{
    auto supported = vk::enumerateInstanceLayerProperties();

    std::vector<const char*> unsupported;
    for (auto& layer : required) {
        auto it = std::find_if(
            supported.begin(),
            supported.end(),
            [&](const vk::LayerProperties& a) { return std::strcmp(a.layerName, layer) == 0; });

        if (it == supported.end()) unsupported.push_back(layer);
    }

    return unsupported;
}

void Vulkan::initLayers()
{
    ZK_ASSERT(mEnabledLayers.empty());

    std::vector<const char*> required;
    if (mDebug) {
        mApp.logger().log(Logger::Level::DEBUG, "requesting vulkan validation layers");
        const char* validation = "VK_LAYER_KHRONOS_validation";
        if (std::find_if(
                required.begin(),
                required.end(),
                [&](const char* ext) { return std::strcmp(validation, ext) == 0; })
            == required.end())
            required.push_back(validation);
    }

    auto unsupported = checkLayersSupport(required);
    if (!unsupported.empty())
        throw Exception("some vulkan layers are not supported");

    mEnabledLayers = required;
}
}  // namespace zk
