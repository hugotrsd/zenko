#pragma once

#include <zenko/common/non_copyable.hpp>

#include <vulkan/vulkan.hpp>

#include <vector>

namespace zk
{
class Application;
class RendererCTXImplVulkan;
class SurfaceImplVulkan;
class VulkanSwapchain final : private NonCopyable
{
  public:
    explicit VulkanSwapchain(
        const Application& app,
        const RendererCTXImplVulkan& rendererCTX,
        const SurfaceImplVulkan& surface,
        const vk::Extent2D& extent2D);
    virtual ~VulkanSwapchain();

  private:
    vk::SurfaceFormatKHR selectSurfaceFormat(const std::vector<vk::SurfaceFormatKHR>& formats) const noexcept;
    vk::PresentModeKHR selectPresentMode(const std::vector<vk::PresentModeKHR>& modes, bool vSync) const noexcept;
    vk::Extent2D selectExtent(const vk::SurfaceCapabilitiesKHR& capabilities, const vk::Extent2D& windowExtent) const noexcept;

  private:
    const Application& mApp;
    const SurfaceImplVulkan& mSurface;
    const RendererCTXImplVulkan& mRendererCTX;

    vk::SwapchainKHR mHandle;
    vk::Format mFormat;
    vk::Extent2D mExtent;

    std::vector<vk::Image> mImages;
    std::vector<vk::ImageView> mImageViews;
};
}  // namespace zk
