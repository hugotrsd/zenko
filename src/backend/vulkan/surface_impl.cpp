#include "backend/vulkan/surface_impl.hpp"
#include "backend/vulkan/vulkan.hpp"
#include "main.hpp"

#include <zenko/core/application.hpp>
#include <zenko/core/logger.hpp>

namespace zk
{
SurfaceImplVulkan::SurfaceImplVulkan(const Application& app, const Vulkan& vulkan, const vk::SurfaceKHR& surface)
    : Surface(app), mVulkan(vulkan), mVkSurface(surface)
{
    ZK_ASSERT(mVkSurface);

    mApp.logger().log(Logger::Level::TRACE, "creating vulkan surface");
}

SurfaceImplVulkan::~SurfaceImplVulkan()
{
    ZK_ASSERT(mVkSurface);

    mApp.logger().log(Logger::Level::TRACE, "destroying vulkan surface");

    mVulkan.handle().destroySurfaceKHR(mVkSurface);
}

const vk::SurfaceKHR& SurfaceImplVulkan::handle() const noexcept
{
    ZK_ASSERT(mVkSurface);
    return mVkSurface;
}
}  // namespace zk
