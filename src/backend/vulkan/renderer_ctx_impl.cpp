#include "backend/vulkan/renderer_ctx_impl.hpp"
#include "backend/vulkan/rendering_api_impl.hpp"
#include "backend/vulkan/surface_impl.hpp"
#include "backend/vulkan/vulkan.hpp"
#include "backend/vulkan/vulkan_device.hpp"
#include "backend/vulkan/vulkan_swapchain.hpp"
#include "main.hpp"

#include <zenko/common/exception.hpp>
#include <zenko/core/application.hpp>
#include <zenko/core/logger.hpp>

namespace zk
{
RendererCTXImplVulkan::RendererCTXImplVulkan(const Application& app, const SurfaceImplVulkan& surface, const vk::Extent2D& extent2D)
    : RendererCTX(app),
      mVulkan(static_cast<const RenderingAPIImplVulkan&>(mApp.renderingAPI()).vulkan()),
      mSurface(surface)
{
    mApp.logger().log(Logger::Level::TRACE, "creating vulkan renderer context");

    mDevice    = std::make_unique<VulkanDevice>(mApp, mVulkan, mSurface);
    mSwapchain = std::make_unique<VulkanSwapchain>(mApp, *this, mSurface, extent2D);
}

RendererCTXImplVulkan::~RendererCTXImplVulkan()
{
    mApp.logger().log(Logger::Level::TRACE, "destroying vulkan renderer context");

    mSwapchain.reset();
    mDevice.reset();
}

const VulkanDevice& RendererCTXImplVulkan::device() const noexcept
{
    ZK_ASSERT(mDevice != nullptr);
    return *mDevice;
}

const VulkanSwapchain& RendererCTXImplVulkan::swapchain() const noexcept
{
    ZK_ASSERT(mSwapchain != nullptr);
    return *mSwapchain;
}
}  // namespace zk
