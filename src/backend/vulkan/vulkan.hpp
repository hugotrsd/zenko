#pragma once

#include <zenko/common/non_copyable.hpp>

#include <vulkan/vulkan.hpp>

#include <vector>

namespace zk
{
class Application;
class Vulkan final : private NonCopyable
{
  public:
    explicit Vulkan(const Application& app, const std::vector<const char*>& extensions);
    virtual ~Vulkan();

    const vk::Instance& handle() const noexcept;

    const std::vector<const char*>& enabledLayers() const noexcept;
    const std::vector<const char*>& enabledExtensions() const noexcept;

  private:
    void initLayers();
    void initExtensions(const std::vector<const char*>& extensions);
    void initDebugMessengers();
    void destroyDebugMessengers() noexcept;

    vk::DebugUtilsMessengerCreateInfoEXT makeDebugMessenger(void* pUserdata) const noexcept;
    static VKAPI_ATTR VkBool32 VKAPI_CALL debugMessengersCallback(
        VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
        VkDebugUtilsMessageTypeFlagsEXT messageType,
        const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
        void* pUserData);

  private:
    const Application& mApp;
    vk::Instance mHandle;
    bool mDebug = false;

    std::vector<const char*> mEnabledLayers;
    std::vector<const char*> mEnabledExtensions;
    std::vector<vk::DebugUtilsMessengerEXT> mDebugMessengers;
};
}  // namespace zk
