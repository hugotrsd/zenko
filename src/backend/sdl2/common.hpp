#pragma once

namespace zk
{

[[noreturn]] void throwSDL2(const char* error);

}  // namespace zk
