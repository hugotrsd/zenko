#include "backend/sdl2/application_impl.hpp"
#include "backend/sdl2/common.hpp"
#include "backend/vulkan/rendering_api_impl.hpp"
#include "main.hpp"

#include <zenko/common/exception.hpp>
#include <zenko/core/logger.hpp>

#include <fmt/core.h>
#include <fmt/ranges.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_vulkan.h>

namespace zk
{
ApplicationImplSDL2::ApplicationImplSDL2(int argc, const char* argv[])
{
    ZK_ASSERT(SDL_WasInit(0) == 0);
    ZK_ASSERT(mAPI == nullptr);

    logger().log(Logger::Level::TRACE, "creating SDL2 application");

    if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
        throwSDL2("failed to initialize SDL2");
    }

    logger().log(Logger::Level::TRACE, "initialized SDL2 subsystems");

    // TODO: select API
    auto impl = RenderingAPI::Impl::VULKAN;
    switch (impl) {
        case RenderingAPI::Impl::VULKAN: {
            if (SDL_Vulkan_LoadLibrary(NULL) != 0) {
                throwSDL2("failed to load vulkan library");
            }

            logger().log(Logger::Level::TRACE, "loaded vulkan library");

            // Retrieve required instance extensions
            unsigned int extCount = 0;
            if (SDL_Vulkan_GetInstanceExtensions(NULL, &extCount, NULL) != SDL_TRUE) {
                throwSDL2("failed to get required vulkan extensions names");
            }

            std::vector<const char*> extNames(extCount);
            if (SDL_Vulkan_GetInstanceExtensions(NULL, &extCount, extNames.data()) != SDL_TRUE) {
                throwSDL2("failed to get required vulkan extensions names");
            }

            mAPI = std::make_unique<RenderingAPIImplVulkan>(*this, extNames);
            break;
        }

        default:
            throw Exception("failed to find suitable rendering API implementation");
    }

    ZK_ASSERT(mAPI);
}

ApplicationImplSDL2::~ApplicationImplSDL2()
{
    ZK_ASSERT(SDL_WasInit(0) != 0);
    ZK_ASSERT(mAPI != nullptr);

    logger().log(Logger::Level::TRACE, "destroying SDL2 application");

    auto impl = mAPI->impl();

    mAPI.reset();

    switch (impl) {
        case RenderingAPI::Impl::VULKAN: {
            SDL_Vulkan_UnloadLibrary();
            logger().log(Logger::Level::TRACE, "unloaded vulkan library");
            break;
        }

        default:
            logger().log(Logger::Level::ERROR, "failed to cleanly destroy application implementation");
            break;
    }

    SDL_Quit();
}

const RenderingAPI& ApplicationImplSDL2::renderingAPI() const noexcept
{
    ZK_ASSERT(mAPI);
    return *mAPI;
}
}  // namespace zk
