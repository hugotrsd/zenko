#include "backend/sdl2/renderer_impl.hpp"

#include <zenko/core/application.hpp>
#include <zenko/core/logger.hpp>
#include <zenko/core/renderable.hpp>

#include <SDL2/SDL.h>
#include <SDL2/SDL_vulkan.h>

namespace zk
{
RendererImplSDL2::RendererImplSDL2(const Application& app)
    : Renderer(app)
{
    mApp.logger().log(Logger::Level::TRACE, "creating SDL2 renderer");
}

RendererImplSDL2::~RendererImplSDL2()
{
    mApp.logger().log(Logger::Level::TRACE, "destroying SDL2 renderer");
}

void RendererImplSDL2::attach(const Renderable& renderable)
{
    // TODO
    /*
    switch (mAPI->impl()) {
        default:
            break;
    }
    */
}

void RendererImplSDL2::detach()
{
}
}  // namespace zk
