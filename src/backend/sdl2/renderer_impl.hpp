#pragma once

#include <zenko/core/renderer.hpp>

namespace zk
{
class RendererImplSDL2 final : public Renderer
{
  public:
    explicit RendererImplSDL2(const Application& app);
    virtual ~RendererImplSDL2();

    virtual void attach(const Renderable& renderable);
    virtual void detach();
};
}  // namespace zk
