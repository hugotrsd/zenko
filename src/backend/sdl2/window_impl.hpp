#pragma once

#include <zenko/core/window.hpp>

#include <SDL2/SDL.h>

#include <memory>
#include <string>

namespace zk
{
class WindowImplSDL2 final : public Window
{
  public:
    explicit WindowImplSDL2(const Application& app, const Properties& props);
    virtual ~WindowImplSDL2();

    virtual const RendererCTX& rendererCTX() const noexcept;
    virtual const Surface& surface() const noexcept;

  private:
    SDL_Window* mWindow = nullptr;
    std::unique_ptr<RendererCTX> mRendererCTX;
    std::unique_ptr<Surface> mSurface;
};
}  // namespace zk
