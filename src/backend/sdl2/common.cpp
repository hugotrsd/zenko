#include "backend/sdl2/common.hpp"
#include "main.hpp"

#include <zenko/common/exception.hpp>

#include <fmt/core.h>

#include <SDL2/SDL_error.h>

namespace zk
{
void throwSDL2(const char* error)
{
    const char* sdl = SDL_GetError();
    ZK_ASSERT(sdl != nullptr);

    // Neasting SDL exception inside another to keep both error messages
    try {
        throw Exception(fmt::format("SDL2 error: {}", sdl));
    } catch (const Exception&) {
        throw Exception(error);
    }
}
}  // namespace zk
