#pragma once

#include <zenko/core/application.hpp>

#include <memory>

namespace zk
{
class ApplicationImplSDL2 final : public Application
{
  public:
    explicit ApplicationImplSDL2(int argc, const char* argv[]);
    virtual ~ApplicationImplSDL2();

    inline virtual constexpr Impl impl() const noexcept;
    virtual const RenderingAPI& renderingAPI() const noexcept;

  private:
    std::unique_ptr<RenderingAPI> mAPI;
};

Application::Impl constexpr ApplicationImplSDL2::impl() const noexcept { return Application::Impl::SDL2; }
}  // namespace zk
