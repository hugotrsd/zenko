#include "backend/sdl2/window_impl.hpp"
#include "backend/sdl2/common.hpp"
#include "backend/vulkan/renderer_ctx_impl.hpp"
#include "backend/vulkan/rendering_api_impl.hpp"
#include "backend/vulkan/surface_impl.hpp"
#include "backend/vulkan/vulkan.hpp"
#include "main.hpp"

#include <zenko/common/exception.hpp>
#include <zenko/core/application.hpp>
#include <zenko/core/logger.hpp>

#include <vulkan/vulkan.hpp>

#include <SDL2/SDL.h>
#include <SDL2/SDL_vulkan.h>

namespace zk
{
WindowImplSDL2::WindowImplSDL2(const Application& app, const Properties& props)
    : Window(app)
{
    ZK_ASSERT(mWindow == nullptr);

    mApp.logger().log(Logger::Level::TRACE, "creating SDL2 window");

    switch (mApp.renderingAPI().impl()) {
        case RenderingAPI::Impl::VULKAN: {
            auto& apiVulkan = static_cast<const RenderingAPIImplVulkan&>(mApp.renderingAPI());

            uint32_t flags = 0;
            flags |= props.fullscreen ? SDL_WINDOW_FULLSCREEN : 0;
            flags |= props.maximized ? SDL_WINDOW_MAXIMIZED : 0;
            flags |= props.borderless ? SDL_WINDOW_BORDERLESS : 0;
            mWindow = SDL_CreateWindow(
                props.title.c_str(),
                SDL_WINDOWPOS_UNDEFINED,
                SDL_WINDOWPOS_UNDEFINED,
                props.width,
                props.height,
                flags | SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE | SDL_WINDOW_VULKAN);

            if (mWindow == nullptr) {
                throwSDL2("failed to create SDL2 vulkan window");
            }

            // Surface creation is handled by SDL, our class is only a wrapper around it
            // Surface destruction is not handled by SDL, our class should implement that
            VkSurfaceKHR surface;
            if (SDL_Vulkan_CreateSurface(mWindow, vk::Instance::CType(apiVulkan.vulkan().handle()), &surface) != SDL_TRUE) {
                SDL_DestroyWindow(mWindow);
                throwSDL2("failed to create SDL2 vulkan surface");
            }

            mSurface = std::make_unique<SurfaceImplVulkan>(mApp, apiVulkan.vulkan(), surface);

            int pixelWidth = 0, pixelHeight = 0;
            SDL_Vulkan_GetDrawableSize(mWindow, &pixelWidth, &pixelHeight);

            mRendererCTX = std::make_unique<RendererCTXImplVulkan>(
                mApp,
                static_cast<const SurfaceImplVulkan&>(*mSurface),
                vk::Extent2D(pixelWidth, pixelHeight)
            );

            break;
        }

        default:
            throw Exception("failed find suitable SDL2 window implementation");
    }

    ZK_ASSERT(mWindow != nullptr);
    ZK_ASSERT(mSurface != nullptr);
    ZK_ASSERT(mRendererCTX != nullptr);
}

WindowImplSDL2::~WindowImplSDL2()
{
    ZK_ASSERT(mWindow != nullptr);

    mApp.logger().log(Logger::Level::TRACE, "destroying SDL2 window");

    mRendererCTX.reset();
    mSurface.reset();

    SDL_DestroyWindow(mWindow);
}

const RendererCTX& WindowImplSDL2::rendererCTX() const noexcept
{
    ZK_ASSERT(mRendererCTX != nullptr);
    return *mRendererCTX;
}

const Surface& WindowImplSDL2::surface() const noexcept
{
    ZK_ASSERT(mSurface != nullptr);
    return *mSurface;
}
}  // namespace zk
