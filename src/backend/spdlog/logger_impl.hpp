#pragma once

#include <zenko/core/logger.hpp>

#include <spdlog/spdlog.h>

#include <memory>
#include <string>

namespace zk
{
class Exception;
class LoggerImpl final : public Logger
{
  public:
    explicit LoggerImpl(const std::string& name);
    virtual ~LoggerImpl();

    const spdlog::logger& impl() const noexcept;
    virtual Level level() const noexcept;
    virtual const std::string& name() const noexcept;

    virtual void setLevel(Level level);

    virtual void log(Level level, const std::string& message) const noexcept;
    virtual void log(Level level, const Exception& ex) const noexcept;

  private:
    static spdlog::level::level_enum convertLevel(Level level);
    static Level convertLevel(spdlog::level::level_enum level);

  private:
    std::unique_ptr<spdlog::logger> mLogger;
};
}  // namespace zk
