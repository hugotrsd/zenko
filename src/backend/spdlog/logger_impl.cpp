#include "backend/spdlog/logger_impl.hpp"
#include "main.hpp"

#include <zenko/common/exception.hpp>

#include <fmt/core.h>

#include <spdlog/sinks/basic_file_sink.h>
#include <spdlog/sinks/stdout_color_sinks.h>

#include <functional>
#include <iostream>
#include <stdexcept>

namespace zk
{
LoggerImpl::LoggerImpl(const std::string& name)
{
    if (name.empty()) { throw Exception("logger name cannot be empty"); }

    try {
        // TODO: file sink
        auto console_sink = std::make_shared<spdlog::sinks::stdout_color_sink_mt>();
        console_sink->set_level(spdlog::level::trace);
        spdlog::sinks_init_list sinks_list = {console_sink};

        mLogger = std::make_unique<spdlog::logger>(name, sinks_list.begin(), sinks_list.end());

        mLogger->set_level(spdlog::level::trace);
        mLogger->set_pattern("%^[%Y-%m-%dT%H:%M:%S%z] (%L) <%n:%t>%$ %v");

    } catch (const spdlog::spdlog_ex& e) {
        throw Exception("failed to initialize spdlog logger");
    }
}

LoggerImpl::~LoggerImpl()
{
}

const spdlog::logger& LoggerImpl::impl() const noexcept
{
    ZK_ASSERT(mLogger);
    return *mLogger;
}

Logger::Level LoggerImpl::level() const noexcept
{
    return convertLevel(mLogger->level());
}

const std::string& LoggerImpl::name() const noexcept
{
    return mLogger->name();
}

void LoggerImpl::setLevel(Level level)
{
    try {
        mLogger->set_level(convertLevel(level));
    } catch (const spdlog::spdlog_ex& e) {
        throw Exception("failed to set log level");
    }
}

void LoggerImpl::log(Level level, const std::string& message) const noexcept
{
    try {
        mLogger->log(convertLevel(level), message);
    } catch (const spdlog::spdlog_ex& e) {
        std::cerr << "failed to log message: " << e.what() << "\n";
    }
}

void LoggerImpl::log(Level level, const Exception& ex) const noexcept
{
    try {
        // Lambda called recursively to unpack nested exceptions
        std::function<void(const Exception&, bool)> logRec = [&](const Exception& exception, bool first) {
            const char* msg = first ? "exception" : "caused by";
            mLogger->log(convertLevel(level), fmt::format("{}: {}", msg, exception.what()));

            try {
                if (exception.cause() != nullptr) {
                    std::rethrow_exception(exception.cause());
                }

            } catch (const Exception& nested) {
                logRec(nested, false);

            } catch (const std::exception& nested) {
                mLogger->log(convertLevel(level), fmt::format("{}: {}", msg, nested.what()));
            }
        };

        logRec(ex, true);

    } catch (const spdlog::spdlog_ex& e) {
        std::cerr << "failed to log exception: " << e.what() << "\n";
    }
}

spdlog::level::level_enum LoggerImpl::convertLevel(Level level)
{
    switch (level) {
        case Level::TRACE: return spdlog::level::trace;
        case Level::DEBUG: return spdlog::level::debug;
        case Level::INFO: return spdlog::level::info;
        case Level::WARNING: return spdlog::level::warn;
        case Level::ERROR: return spdlog::level::err;
        case Level::CRITICAL: return spdlog::level::critical;
        case Level::OFF: return spdlog::level::off;
        default: throw Exception("invalid log level");
    }
}

Logger::Level LoggerImpl::convertLevel(spdlog::level::level_enum level)
{
    switch (level) {
        case spdlog::level::trace: return Level::TRACE;
        case spdlog::level::debug: return Level::DEBUG;
        case spdlog::level::info: return Level::INFO;
        case spdlog::level::warn: return Level::WARNING;
        case spdlog::level::err: return Level::ERROR;
        case spdlog::level::critical: return Level::CRITICAL;
        case spdlog::level::off: return Level::OFF;
        default: throw Exception("invalid log level");
    }
}
}  // namespace zk
