#include "main.hpp"

#include <zenko/core/application.hpp>
#include <zenko/core/logger.hpp>
#include <zenko/core/rendering_api.hpp>

#include <vector>

namespace zk
{
RenderingAPI::RenderingAPI(const Application& app)
    : mApp(app)
{
    mApp.logger().log(Logger::Level::TRACE, "creating renderer API");
}

RenderingAPI::~RenderingAPI()
{
    mApp.logger().log(Logger::Level::TRACE, "destroying renderer API");
}
}  // namespace zk
