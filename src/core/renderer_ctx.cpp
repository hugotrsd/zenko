#include <zenko/core/application.hpp>
#include <zenko/core/logger.hpp>
#include <zenko/core/renderer_ctx.hpp>

namespace zk
{
RendererCTX::RendererCTX(const Application& app)
    : mApp(app)
{
    mApp.logger().log(Logger::Level::TRACE, "creating renderer context");
}

RendererCTX::~RendererCTX()
{
    mApp.logger().log(Logger::Level::TRACE, "destroying renderer context");
}
}  // namespace zk
