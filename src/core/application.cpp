#include "backend/sdl2/application_impl.hpp"
#include "main.hpp"

#include <zenko/common/exception.hpp>
#include <zenko/core/application.hpp>
#include <zenko/core/logger.hpp>

namespace zk
{
std::mutex Application::sMutex;

std::unique_ptr<Application> Application::create(int argc, const char* argv[])
{
    // TODO: choose appropriate backend
    auto impl = Application::Impl::SDL2;
    switch (impl) {
        case Application::Impl::SDL2:
            return std::make_unique<ApplicationImplSDL2>(argc, argv);

        default:
            throw Exception("failed to find suitable application implementation");
    }
}

Application::Application()
    : mLock(sMutex),  // TODO: this is good for now, but we can do better
      mLogger(Logger::create("core"))
{
    ZK_ASSERT(mLogger != nullptr);
    mLogger->log(Logger::Level::TRACE, "creating application");
}

Application::~Application()
{
    ZK_ASSERT(mLogger != nullptr);
    mLogger->log(Logger::Level::TRACE, "destroying application");
}

const Logger& Application::logger() const noexcept
{
    ZK_ASSERT(mLogger);
    return *mLogger;
}
}  // namespace zk
