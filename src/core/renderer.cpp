#include "main.hpp"

#include <zenko/core/application.hpp>
#include <zenko/core/logger.hpp>
#include <zenko/core/renderer.hpp>

namespace zk
{
Renderer::Renderer(const Application& app)
    : mApp(app)
{
    mApp.logger().log(Logger::Level::TRACE, "creating renderer");
}

Renderer::~Renderer()
{
    mApp.logger().log(Logger::Level::TRACE, "destroying renderer");
}
}  // namespace zk
