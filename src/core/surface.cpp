#include <zenko/core/application.hpp>
#include <zenko/core/logger.hpp>
#include <zenko/core/surface.hpp>

namespace zk
{
Surface::Surface(const Application& app)
    : mApp(app)
{
    mApp.logger().log(Logger::Level::TRACE, "creating surface");
}

Surface::~Surface()
{
    mApp.logger().log(Logger::Level::TRACE, "destroying surface");
}
}  // namespace zk
