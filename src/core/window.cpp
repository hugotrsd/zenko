#include "backend/sdl2/window_impl.hpp"
#include "main.hpp"

#include <zenko/common/exception.hpp>
#include <zenko/core/application.hpp>
#include <zenko/core/logger.hpp>
#include <zenko/core/window.hpp>

namespace zk
{
std::unique_ptr<Window> Window::create(const Application& app, const Properties& props)
{
    switch (app.impl()) {
        case Application::Impl::SDL2:
            return std::make_unique<WindowImplSDL2>(app, props);
        default:
            throw Exception("failed to find a suitable window implementation");
    }
}

Window::Window(const Application& app)
    : mApp(app)
{
    mApp.logger().log(Logger::Level::TRACE, "creating window");
}

Window::~Window()
{
    mApp.logger().log(Logger::Level::TRACE, "destroying window");
}
}  // namespace zk
