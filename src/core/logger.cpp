#include "backend/spdlog/logger_impl.hpp"

#include <zenko/common/exception.hpp>
#include <zenko/core/logger.hpp>

namespace zk
{
std::unique_ptr<Logger> Logger::create(const std::string& name)
{
    if (name.empty()) {
        throw Exception("logger name is empty");
    }

    return std::make_unique<LoggerImpl>(name);
}

Logger::Logger()
{
}

Logger::~Logger()
{
}
}  // namespace zk
