#pragma once

#include <zenko/core/renderer_target.hpp>

#include <vulkan/vulkan.hpp>

namespace zk
{
class Vulkan;
class VulkanDevice;
class RendererTarget::ImplVulkan final : public RendererTarget
{
  public:
    explicit ImplVulkan(const Application& app, const Vulkan& vulkan, const vk::SurfaceKHR& surface);
    virtual ~ImplVulkan();

  private:
    const Vulkan& mVulkan;
    const vk::SurfaceKHR& mSurface;
    std::unique_ptr<VulkanDevice> mDevice;
};
}  // namespace zk
