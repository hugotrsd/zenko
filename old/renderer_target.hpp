#pragma once

namespace zk
{
class Application;
class RendererTarget
{
  public:
    class ImplVulkan;

  public:
    virtual ~RendererTarget();

  protected:
    explicit RendererTarget(const Application& app);

  protected:
    const Application& mApp;
};
}  // namespace zk
