#include <zenko/core/application.hpp>
#include <zenko/core/logger.hpp>
#include <zenko/core/renderer_target.hpp>

namespace zk
{
RendererTarget::RendererTarget(const Application& app)
    : mApp(app)
{
    mApp.logger().log(Logger::Level::TRACE, "creating renderer target");
}

RendererTarget::~RendererTarget()
{
    mApp.logger().log(Logger::Level::TRACE, "destroying renderer target");
}
}  // namespace zk
