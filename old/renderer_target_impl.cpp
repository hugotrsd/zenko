#include "backend/vulkan/renderer_target_impl.hpp"
#include "backend/vulkan/vulkan.hpp"
#include "backend/vulkan/vulkan_device.hpp"
#include "main.hpp"

#include <zenko/core/application.hpp>
#include <zenko/core/logger.hpp>

namespace zk
{
RendererTarget::ImplVulkan::ImplVulkan(const Application& app, const Vulkan& vulkan, const vk::SurfaceKHR& surface)
    : RendererTarget(app), mVulkan(vulkan), mSurface(surface)
{
    ZK_ASSERT(surface);

    mDevice = std::make_unique<VulkanDevice>(mApp, mVulkan, mSurface);

    mApp.logger().log(Logger::Level::TRACE, "creating vulkan renderer target");
}

RendererTarget::ImplVulkan::~ImplVulkan()
{
    mApp.logger().log(Logger::Level::TRACE, "destroying vulkan renderer target");

    mDevice.reset();
}
}  // namespace zk
